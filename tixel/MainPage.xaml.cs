﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Devices.Sensors;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace tixel
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Clock clock;
        SimpleOrientationSensor sensor;
        public MainPage()
        {
            this.InitializeComponent();
            sensor = SimpleOrientationSensor.GetDefault();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //clock.Start();
            clock = new Clock(clockCanvas);
        }

        void SizeChangedHandler(object sender, SizeChangedEventArgs e)
        {
            ApplicationViewState viewState = Windows.UI.ViewManagement.ApplicationView.Value;
            int size = viewState == ApplicationViewState.FullScreenLandscape ? 50 : 30;
            Digit.Resize(size);
            if (clock != null)
            {
                clock.Draw();
            }
        }


    }
}
