﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;
using Windows.UI;
using Windows.UI.Xaml.Media;
using System.Diagnostics;
using Windows.UI.Xaml;
namespace tixel
{
    class Digit
    {
        private int _number;
        public int number {
            get { return this._number; }
            set { 
                this._number = value;
            }
        }
        public int index;
        public Canvas c;
        public Color color;
        public int numColumns;

        public static int size = 50;
        public static int padding = 25;
        public static int digitSpace = 50;

        public static void Resize(int newSize)
        {
            size = newSize;
            digitSpace = newSize;
            padding = newSize / 2;
        }

        public Digit(int i, Canvas c, Color color, int numColumns)
        {
            _number = 0;
            this.c = c;
            this.index = i;
            this.color = color;
            this.numColumns = numColumns;
        }

        //returns the right most coordinate
        public void Redraw(ref int left)
        {
            List<bool> litList = GenerateLitList();
            int count = 0;
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < numColumns; col++)
                {
                    DrawRect(CreateRect(), left, row, col, litList[count]);
                    count++;
                }
            }
            left += numColumns * (size + padding) + digitSpace;
        }

        private List<bool> GenerateLitList()
        {
            List<bool> lit = new List<bool>();
            for (int i = 0; i < 3 * numColumns; i++)
            {
                if (i < _number)
                {
                    lit.Add(true);
                }
                else
                {
                    lit.Add(false);
                }
            }
            return lit.OrderBy(a => Guid.NewGuid()).ToList<bool>();
        }

        private Rectangle CreateRect()
        {
            Rectangle r = new Rectangle();
            r.Width = size;
            r.Height = size;
            return r;
        }

        private int DrawRect(Rectangle r, int left, int row, int col, bool light)
        {
            int realLeft = CalcLeft(left, col);
            int realTop = CalcTop(row);
            Canvas.SetLeft(r, realLeft);
            Canvas.SetTop(r, realTop);
            Color color = light ? this.color : Colors.Black;
            r.Fill = new SolidColorBrush(color);
            r.Stroke = new SolidColorBrush(Colors.White);
            c.Children.Add(r);
            return realLeft + size + padding;
        }

        private int CalcLeft(int left, int col)
        {
            return left + (col * (size + padding));
        }

        private int CalcTop(int row)
        {
            int realTop = (int)((Window.Current.Bounds.Height / 2) - (padding + 1.5 * size));
            return realTop + (row * (size + padding));
        }

    }
}
