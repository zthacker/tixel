﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;
using Windows.UI;

namespace tixel
{
    class Clock
    {
        private int hours, minutes;
        private Digit hoursTens, hoursOnes, minutesTens, minutesOnes;
        private DispatcherTimer redrawTimer, secondTimer;
        private Canvas canvas;
        private bool go = false;
        private const int totalNumColumns = 9;
        private const int numDigits = 4;
        private int totalWidth;
        public Clock(Canvas c)
        {
            canvas = c;
            hoursTens = new Digit(0, canvas, Colors.Red, 1);
            hoursOnes = new Digit(1, canvas, Colors.Blue, 3);
            minutesTens = new Digit(2, canvas, Colors.Green, 2);
            minutesOnes = new Digit(3, canvas, Colors.Purple, 3);
            CheckTime();
            redrawTimer = CreateRedrawTimer();
            secondTimer = CreateSecondTimer();
            redrawTimer.Start();
            secondTimer.Start();
            Draw();
        }

        private void CalculateTotalWidth()
        {
            totalWidth = totalNumColumns * Digit.size + (totalNumColumns - 1) * Digit.padding;
            totalWidth += (numDigits - 1) * Digit.digitSpace;
        }

        private DispatcherTimer CreateRedrawTimer()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += redrawTimer_Tick;
            timer.Interval = new TimeSpan(0, 0, 3);
            return timer;
        }

        private DispatcherTimer CreateSecondTimer()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += secondTimer_Tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            return timer;
        }

        void redrawTimer_Tick(object sender, object e)
        {
            Draw();
        }

        public void Draw()
        {
            EmptyCanvas();
            CalculateTotalWidth();
            int left = (int)Window.Current.Bounds.Width / 2 - totalWidth / 2;
            hoursTens.Redraw(ref left);
            hoursOnes.Redraw(ref left);
            minutesTens.Redraw(ref left);
            minutesOnes.Redraw(ref left);
        }

        private void EmptyCanvas()
        {
            List<Rectangle> rects = canvas.Children.OfType<Rectangle>().ToList();
            foreach (Rectangle rect in rects)
            {
                canvas.Children.Remove(rect);
            }
        }

        void secondTimer_Tick(object sender, object e)
        {
            CheckTime();
        }

        private void CheckTime()
        {
            hours = int.Parse(DateTime.Now.ToString("%h"));
            minutes = DateTime.Now.Minute;
            hoursTens.number = (hours / 10);
            hoursOnes.number = (hours % 10);
            minutesTens.number = (minutes / 10);
            minutesOnes.number = (minutes % 10);
        }
    }
}
